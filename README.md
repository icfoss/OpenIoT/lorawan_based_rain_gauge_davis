# LoRaWAN based Raingauge Station

The Rain Gauge Station – Model 3424 generates real-time data to help in flood warning, reservoir management, and any other application that calls for timely rainfall information.When there is a chance of flooding, the information from the Rain Gauges helps emergency management experts make decisions that can save both lives and property. These stations are typically used in weather, meteorology, and mesonet applications. The Raingauge station developed using [Davis rainguage](https://www.davisinstruments.com/products/aerocone-rain-collector-with-flat-base-for-vantage-pro2?_pos=3&_sid=2be3af584&_ss=r) with a battery-solar powered.
![Raingauge](https://gitlab.com/icfoss/OpenIoT/lorawan_based_rain_gauge_davis/-/raw/master/Images/photo_2022-10-13_10-47-53.jpg) *Raingauge at ICFOSS, Greenfield Stadium*

# List of Parameters recorded

- Rain measurement
- Every 15 minutes rainfall data
- Daily Rainfall data(Transmits every day 8.00 AM)

LoRaWAN based Automatic Rain Gauge Station is a system which measure the Total rainfall and transmits the data at regular intervals.
List of Parameters recorded

The application code is written on top of the I-cube-lrwan package by STM32. 

# Prerequisites
  - STM32 Cube IDE [Tested]
  - [C1_dev_v1.0](https://gitlab.com/icfoss/OpenIoT/c1_dev_v1.0)
  - Solar Charge Controller
  - Solar Panel (3V, 6V voc)
  - [Raing guage Davis](https://www.davisinstruments.com/products/aerocone-rain-collector-with-flat-base-for-vantage-pro2?_pos=3&_sid=2be3af584&_ss=r)

# Getting Started
  - Make sure that you have a [C1_dev_v1.0](https://gitlab.com/icfoss/OpenIoT/c1_dev_v1.0) or any STM32 Board.
  - Connect the components as shown in the [Wiring Diagram](https://gitlab.com/icfoss/OpenIoT/lorawan_based_rain_gauge_davis/-/blob/master/Hardware/Rain%20Gauge.pdf)
  - Install STM32 Cube IDE.
  - Select board : B-L072Z-LRWAN1
  - For programming connect C1_dev_v1.0 through B-L072Z-LRWAN1 [here](https://gitlab.com/icfoss/OpenIoT/lorawan_based_rain_gauge_davis/-/blob/master/Hardware/Pgm_conection_STM32board.pdf).
  - For programming connect C1_dev_v1.0 through STM32 programmer Module [here](https://gitlab.com/icfoss/OpenIoT/lorawan_based_rain_gauge_davis/-/blob/master/Hardware/Pgm_connection_STMProgrammer.pdf).
  - If using ABP(Activation by Personalisation) method, Change the address/keys of the device address, network session key, application session key.
  - If using OTAA (Over the Air Activation) method, Change the address/keys of the DUI (device EUI), application key.

# Contributing
Instructions coming up soon.

# License
This project is licensed under the MIT License - see the LICENSE.md file for details
